# LED Filter Testing Rig

A parametric design for a test rig for different combinations of LEE or Rosco gel filters allowing LEDs and photodetectors to be set up with different filter combinations and either a cuvette or capillary containing the sample. 

<img src="./images/cuvette_rig.jpg" width="30%">

