use <LEGO.scad>;

hole_length = 70;
hole_width = 30;
hole_depth = 5;

module bottom(){union() {
    block(
        type="baseplate",
        width=6,
        length=14,
        roadway_width=4,
        roadway_length=12,
        roadway_x=1,
        roadway_y=1,
    stud_type = "solid"
    );
    

   // }
}
}

module frame_hole(){
   difference(){
   bottom(); 
   translate(v = [-hole_length/2, -hole_width/2])
   cube(size = [hole_length,hole_width,hole_depth], center = true/false);
    }
}

frame_hole();